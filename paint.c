/** paint.c
In charge of putting pretty things on the screen. 
It'll need a big overhaul if/when we switch to tiles

*/

#include "paint.h"
#include "mon.h"
#include "map.h"
#include "playerMovement.h"
#include "libtcod/libtcod.h"
#include <time.h>
#include <string.h>
#include <unistd.h>  //Sleep...  where the hell are msleep and usleep?

extern int g_money;

//Debated putting the console in it's own *.c file.  But it's so small  
char g_console[50][80];
int g_consoleIndex=0;


///Player viewable text on the screen. A list of what happened.
///Run every paint command
void consolePrint()
{
  int screenY, i;
  i= g_consoleIndex;
  for(screenY = 26; screenY<MAP_SIZE; screenY++)
  {
    TCOD_console_printf(NULL,MAP_SIZE+1,screenY, "%s", g_console[i]);
    i--; //Had mod op and negatives
    if(i == -1) 
    {
      i = 49;
    }
  }
}

///Adds strings to a rolling array. Clobbers old entries. 
///@param str Text to add to the console.  size limited to 80. 
//TODO, make it variadic like printf.   WHATEVER grab logging and shoe-horn that in. giterdone.
void consoleAdd(const char* str)
{
  sprintf(g_console[g_consoleIndex], "%.80s",str);
  g_consoleIndex = (g_consoleIndex+1)%50;
}




///A graphical object. looks like  herp:[====   ]  
///Color coded at 50% and 25%.
///Expands all the way to the right edge of the screen
///@param str title of the value being displayed
///@param x,y position of the leftmost character, it grows to the right
///@param val, maxVal control the percentage full
void printHUDbarThingy(char* str, int x, int y, int val, int maxVal)
{
  int i,pos;
  TCOD_color_t color;

  color = TCOD_green;
  if(val < maxVal/2)
    color = TCOD_yellow;
  else if(val < maxVal/4)
    color = TCOD_red;

  TCOD_console_printf(NULL,x,y,str);
  TCOD_console_printf(NULL,x+ (int)strlen(str),y,"[");
  for(i=0, pos=x+strlen(str)+1; pos<SCREEN_SIZE_X && i < val; i++, pos++)
  {
    TCOD_console_set_char_background(NULL, pos,y, color, TCOD_BKGND_SET);
    TCOD_console_printf(NULL,pos,y, "=");
  }
  TCOD_console_printf(NULL,x+strlen(str)+maxVal+1,y,"]");
}

void printHUDmonStats(int x, int y, int offset)
{
  //find monster closest to player (ugh, in LOS), offset, and print everything you can about them, starting at X, Y
  int i = monNearestTo(g_mon[MON_PLAYER].x, g_mon[MON_PLAYER].y, offset);

  TCOD_map_compute_fov(g_tcodMap, g_mon[MON_PLAYER].x, g_mon[MON_PLAYER].y, 999, true, FOV_PERMISSIVE_4);
  if( !TCOD_map_is_in_fov(g_tcodMap, g_mon[i].x,g_mon[i].y))
  {
    TCOD_console_printf(NULL,x,y, "Hidden");
    TCOD_console_printf(NULL,x,y+1, "%d %d", offset, i);
    return;
  }

  int line = 0;
  TCOD_console_printf(NULL,x,y+line++, "%s", g_mon[i].name);
  TCOD_console_printf(NULL,x,y+line++, "---");

  TCOD_console_printf(NULL,x,y+line++, "Use the Numpad to navigate ");
  TCOD_console_printf(NULL,x,y+line++, "Jump to the north with 'j'");
  TCOD_console_printf(NULL,x,y+line++, "Reach the next level  '>' ");
  TCOD_console_printf(NULL,x,y+line++, "'u' and 'h' to adjust delay ");
  

  TCOD_console_printf(NULL,x,y+line++, "step: %d Qued: %d ", g_currentStep, g_quedStep);
  TCOD_console_printf(NULL,x,y+line++, "  (%lu msec delay)",g_tickDelay_ms);
  if(g_currentStep != g_quedStep)
  {
    TCOD_console_printf(NULL,x,y+line++, "--** WAIT FOR IT!!! **-- ");
  }
#if DEBUG
  TCOD_console_printf(NULL,x,y+line++, "AI stat: %d ", g_mon[i].aiState);
  TCOD_console_printf(NULL,x,y+line++, "x: %d y: %d ", g_mon[i].x, g_mon[i].y);
  TCOD_console_printf(NULL,x,y+line++, "tX: %d tY: %d ", g_mon[i].targetX, g_mon[i].targetY);
#endif
}



///Displays HP, status, and prettymuch everything that isn't the map
///It's off to the right for now.
///Eventually, the player's view might not be the entire map. IE, split mapsize and screensize
void printHUD()
{
  printHUDmonStats(MAP_SIZE+1, 5 , 0);
  //TCOD_console_printf(NULL, MAP_SIZE+1, 3, "Money: %d", g_money);
  TCOD_console_printf(NULL, MAP_SIZE+1, 4, "Speed: %d",g_mon[MON_PLAYER].speed);

  TCOD_console_printf(NULL, MAP_SIZE+1, 20, "%s: %d",g_bugName[0], g_bug[0]);
  TCOD_console_printf(NULL, MAP_SIZE+1, 21, "%s: %d",g_bugName[1], g_bug[1]);
  TCOD_console_printf(NULL, MAP_SIZE+1, 22, "%s: %d",g_bugName[2], g_bug[2]);
  TCOD_console_printf(NULL, MAP_SIZE+1, 23, "%s: %d",g_bugName[3], g_bug[3]);
  TCOD_console_printf(NULL, MAP_SIZE+1, 24, "%s: %d",g_bugName[4], g_bug[4]);
//TODO: clock
}


///Updates g_tcodMapMonView with the FOV of a specific monster
///Used at the start of most monsters turn if they care about what they see.
///Also used to display the monster's vision arc when painting the screen.
///Copy the map, insert blocking vision depending on their facing, generate FOV
///@param i index of monster
///@output g_tcodMapMonView
void monsterFOV(int i, int directionalView)
{
  int x,y;

  //Effectively clearing the map
  TCOD_map_copy(g_tcodMap,g_tcodMapMonView);

  x = g_mon[i].x;
  y = g_mon[i].y;

  if(directionalView)
  {
    //If facing a dir, block everything behind you
    if(g_mon[i].facing == NORTH)
    {
      TCOD_map_set_properties(g_tcodMapMonView, x-1, y+1, false,false);
      TCOD_map_set_properties(g_tcodMapMonView, x,   y+1, false,false);
      TCOD_map_set_properties(g_tcodMapMonView, x+1, y+1, false,false);
    }
    if(g_mon[i].facing == SOUTH)
    {
      TCOD_map_set_properties(g_tcodMapMonView, x-1, y-1, false,false);
      TCOD_map_set_properties(g_tcodMapMonView, x,   y-1, false,false);
      TCOD_map_set_properties(g_tcodMapMonView, x+1, y-1, false,false);
    }
    if(g_mon[i].facing == EAST)
    {
      TCOD_map_set_properties(g_tcodMapMonView, x-1, y-1, false,false);
      TCOD_map_set_properties(g_tcodMapMonView, x-1, y  , false,false);
      TCOD_map_set_properties(g_tcodMapMonView, x-1, y+1, false,false);
    }
    if(g_mon[i].facing == WEST)
    {
      TCOD_map_set_properties(g_tcodMapMonView, x+1, y-1, false,false);
      TCOD_map_set_properties(g_tcodMapMonView, x+1, y  , false,false);
      TCOD_map_set_properties(g_tcodMapMonView, x+1, y+1, false,false);
    }
  }

  TCOD_map_compute_fov(g_tcodMapMonView, x, y, MON_VISION_DIST, true, FOV_RESTRICTIVE);
}

///The displayed map is from a monster's perspective. 
///@param i index of monster
void printMap_monsterView(int i)
{
  int x,y, inLOS;
  char c;

  monsterFOV(i,1); // sets g_tcodMapMonView

  for(x=0; x<MAP_SIZE; x++)
  {
    for(y=0; y<MAP_SIZE; y++)
    {
      if(g_map[x][y].isWall )
      {
        TCOD_console_set_char(NULL,x,y, '#');
        continue;
      }
      else if(g_map[x][y].special )
      {
        c = g_map[x][y].special;
        TCOD_console_set_char(NULL,x,y, c);
        continue;
      }

      inLOS = TCOD_map_is_in_fov(g_tcodMapMonView, x, y);
      if(inLOS)
      {
        if(g_map[x][y].mon)
          c = g_mon[g_map[x][y].mon].type;
        else
          c = ' ';
        TCOD_console_set_char(NULL,x,y, c);
      }
      else 
      {
        TCOD_console_set_char(NULL,x,y, '.');
      }
    }
  }
}




///Displays the map
///hmmm with VIEW_NIGHT, this becomes REALLY hard to play. 
void printMap_playerView()
{
  int x,y, inLOS;
  char c;

//  TCOD_map_compute_fov(g_tcodMap, g_mon[MON_PLAYER].x, g_mon[MON_PLAYER].y, PLAYER_VISION_DIST, true, FOV_PERMISSIVE_4);
//  if(vis != VIEW_DAY)   Naw not for azure, or Breakneck
//    audioOverlay();

  for(x=0; x<MAP_SIZE; x++)
  {
    for(y=0; y<MAP_SIZE; y++)
    {

      inLOS = true;
      //inLOS = TCOD_map_is_in_fov(g_tcodMap, x,y);
      if(g_map[x][y].isWall )
      {
        TCOD_console_set_char(NULL,x,y, '#');
      }
      else if(!inLOS )
      {
        TCOD_console_set_char(NULL,x,y, '.'); //explored, but out of sight,  the rest assumes inLOS
      }
      else if(g_map[x][y].mon)
      {
        c = g_mon[g_map[x][y].mon].type;        
        TCOD_console_set_char(NULL,x,y, c);
      }
      else if(g_map[x][y].special )
      {
        c = g_map[x][y].special;
        TCOD_console_set_char(NULL,x,y, c);
      }
      else
      {
        TCOD_console_set_char(NULL,x,y, ' '); //Open space
      }
    }
  }
  TCOD_console_set_char(NULL,g_mon[MON_PLAYER].x,g_mon[MON_PLAYER].y, '@');

}


// A: Show were we can move, given commands
// B: Show facing changes       color coded?
// C: Show speed changes
// D: Still gotta display what's underneath... blink it?  
void printPotentialMovement()
{
  //static int blink = 5;  //oooh, no the code halts in user input... 

  for(int key = 0; key<=9; key++)
  {
    TCOD_console_set_char(NULL,g_keyMotions[key].destX,g_keyMotions[key].destY, g_keyMotions[key].keyChar);
  }

  //Whatever, paint the player @ again.   Keep it on top
  TCOD_console_set_char(NULL,g_mon[MON_PLAYER].x,g_mon[MON_PLAYER].y, '@');

}

///Top level paint function. If it gets displayed, it should come from here
///@param turn The current turn
void paintScreen(int turn)
{
  //Clears anything from last time we printed. bg color at this point
  TCOD_console_clear(NULL);

  consolePrint();

  printMap_playerView();

  printHUD();

  printPotentialMovement();

  //Just debugging crap
  //TCOD_console_printf(NULL, MAP_SIZE+1, 1, "Level: %d", chap);
  TCOD_console_printf(NULL, MAP_SIZE+1, 2, "turn: %d", turn);

  TCOD_console_flush();
}

void paintEnding()
{
  //Clears anything from last time we printed. bg color at this point
  TCOD_console_clear(NULL);

  //Some sort of big you lose banner here.   
  //... alternatively, make this whole thing about focusing. You caught your attention 
  //  the guards are...  your self-control?
  //I mean the theme of the game jam is repair
  TCOD_console_printf_ex(NULL, MAP_SIZE/2, MAP_SIZE/2, TCOD_BKGND_DARKEN, TCOD_CENTER, "!CAUGHT!");

  TCOD_console_flush();

  //Delay for X seconds. Play Saaaaaad tune here.
  sleep(3);  //so, kinda lame.  Alternatively, dive into all the raw SDL
  TCOD_console_printf_ex(NULL, 2+MAP_SIZE/2, 2+MAP_SIZE/2, TCOD_BKGND_DARKEN, TCOD_CENTER, "Try Again");
  TCOD_console_flush();
  sleep(1);



}

