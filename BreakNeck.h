#ifndef __BREAKNECK__
#define __BREAKNECK__

#include <stdio.h>

#define TRUE 1
#define FALSE 0 

#define SCREEN_SIZE_X 80
#define SCREEN_SIZE_Y 31

//#define MAP_SIZE 10
//#define MAP_SIZE 24
#define MAP_SIZE 50

//TODO: Move all this to mon.h
#define NUM_GUARD_WAYPOINTS 4
#define MAX_MONSTERS 20          //One is the player. Because deep down, we're all monsters
extern int gNumMonsters;

#define MON_VISION_DIST 20

//Result of user input
#define QUIT          1
#define ADVANCE       2

//Just to help remember wtf the inputs are
#define TRANSPARENT_YES 1
#define TRANSPARENT_NO  0
#define WALKABLE_YES    1
#define WALKABLE_NO     0


#define NORTH      0 
#define NORTHEAST  1
#define EAST       2
#define SOUTHEAST  3
#define SOUTH      4
#define SOUTHWEST  5
#define WEST       6
#define NORTHWEST  7
#define NUM_DIRECTIONS  8   //might switch back to 4

#define FACE_FWD      0
#define FACE_FWD_R    1
#define FACE_RIGHT    2
#define FACE_BACK_R   3
#define FACE_BACK     4
#define FACE_BACK_L   5
#define FACE_LEFT     6
#define FACE_FWD_L    7
#define FACE_COAST    -1
#define FACE_JUMP -2

#define SPEED_STILL 0
#define SPEED_WALK 1
#define SPEED_JOG 2
#define SPEED_RUN 3
#define SPEED_BREAKNECK 4
#define NUM_SPEEDS 5


extern int g_turn;
extern int g_chap;
extern int g_YOUDIED;
extern int g_bug[];
extern char g_bugName[][20];  //ugh, fucking globabls are back

extern long int g_tickDelay_ms;
extern int g_jumping;

//TODO decide if I really want to move this to map.h
typedef struct Map_t
{
  char isWall;     // doubles as wall-type
  char special;    // 0 = nothing
  int mon;         // Originally a pointer, but you know what? Fuck it
}Map_t;





int dist(int x1, int y1, int x2, int y2);

int playerMovementInput(int key);

int advanceTime();
int userInput();
void logging();

void initlibtcod();
void initGameData();

void wtf();

#endif
