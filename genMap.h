#ifndef __BREAKNECK_GENMAP__
#define __BREAKNECK_GENMAP__

int generateRandMap();
int generateBoxyMap(int numBoxes, int boxSize);
int generateCityBlockMap();
void genMapAdditems();

int generateBigRooftopMap(int level);

#endif
