/** ai_guard.c

Controls the state machine of  all of their actions.
but these guards are pretty dumb

Their default state is to wander
TODO: patrol
TODO: specific map module that need the guard to be pulled away, like ramps

If they spot the player, give chase until they lose em

Steady States: Wander, Attack
Transitions:     spot,


 */

#include "ai_guard.h"
#include "map.h"
#include "mon.h"
#include "paint.h"
#include "BreakNeck.h"
#include "libtcod/libtcod.h"
#include <stdlib.h>

#include <unistd.h> //for sleep


//. . . I think the only reason this exists is because at some point I didn't feel like dealing with function pointers. 
void aiGuard(int i)
{
  switch(g_mon[i].aiState)
  {
    case AI_GUARD_WANDER: aiGuardWander(i); break;
    case AI_GUARD_ATTACK: aiGuardAttack(i); break;
    default:
      wtf();
  }
}

///Go to X,Y coordinates as stored in targetX/Y, reacting to anyone you see.
///@param i index of monster
void aiGuardWander(int i) //AI_WANDER
{
  int pickNewSpot = 0;
  int xTarget =  g_mon[i].targetX;
  int yTarget =  g_mon[i].targetY;

  //Can they see other monsters, player included?
  aiGuardSpotCheck(i);
  
  //Probably don't need to create and destroy the path each time.
  TCOD_path_t path = TCOD_path_new_using_map(g_tcodMap,0.0f); 

  if(TCOD_path_compute( path, g_mon[i].x, g_mon[i].y, xTarget, yTarget))
  {
    if(TCOD_path_size(path)>0) 
    {
      int x,y;
      TCOD_path_get(path,0,&x,&y);

      //don't casually attack things if you're just walking around.
      if(g_map[x][y].mon == 0)
      {
        monMove(i, x,y);
      }
      else //pick a new spot. Otherwise there's traffic jams
      {
        pickNewSpot = 1;  
      }
    }
    else //Pick a new spot... fuck dry    logic hard
    {
      pickNewSpot = 1;  
    }

  }
  else
    pickNewSpot = 1;

  TCOD_path_delete(path); 

  //Yep, the generic Fuck This Noise while wandering is just to pick somewhere else. 
  //It really doesn't matter
  if(pickNewSpot)
  {
    int x,y;
    do
    {
      x = rand()%MAP_SIZE;
      y = rand()%MAP_SIZE;
    } while(!isPassable(x,y));
    g_mon[i].targetX = x;
    g_mon[i].targetY = y;
  }

}

void aiGuardAttack(int i)
{
  //Should I be running away?
  aiGuardSpotCheck(i);

  //update target if you see the player
  TCOD_path_t path = TCOD_path_new_using_map(g_tcodMap,0.0f); 
  monsterFOV(i,1); // g_tcodMapMonView

  if(TCOD_map_is_in_fov(g_tcodMapMonView, g_mon[MON_PLAYER].x, g_mon[MON_PLAYER].y))
  {
    g_mon[i].targetX = g_mon[MON_PLAYER].x;
    g_mon[i].targetY = g_mon[MON_PLAYER].y;
  }

  // move towards target
  int x = g_mon[i].targetX;
  int y = g_mon[i].targetY;
  if(TCOD_path_compute( path, g_mon[i].x, g_mon[i].y, x, y))
  {
    if(TCOD_path_size(path) > 0) 
    {
      TCOD_path_get( path,0,&x,&y);
      monMove(i, x,y);
    }
    else
      g_mon[i].aiState = AI_GUARD_WANDER; 
  }       
  TCOD_path_delete(path); 
}


//Is player in view. 
void aiGuardSpotCheck(int i)
{
  monsterFOV(i,1); // g_tcodMapMonView

  if(TCOD_map_is_in_fov(g_tcodMapMonView, g_mon[MON_PLAYER].x, g_mon[MON_PLAYER].y))
  {
	  g_mon[i].targetX = g_mon[MON_PLAYER].x;
	  g_mon[i].targetY = g_mon[MON_PLAYER].y;
	  g_mon[i].aiState = AI_GUARD_ATTACK;
  }

}

//Nothing special
void aiGuardMove(int i, int x, int y)
{
  if(g_mon[MON_PLAYER].x == x && g_mon[MON_PLAYER].y == y) //And not sliding, eventually
  {
    g_YOUDIED = 1; //yeah, getting tired.
  }
}


