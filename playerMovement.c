/** playerMovement.c
  Yeah, this is the interesting part of this 7DRL.  Because we are CRAZY gone from simple WASD.

  But players can build up speed which changes where all their movement command could take them.
  Each key could change their facing and speed
  This is easiest to draw on grid-paper. Go look at that. 

  We ALSO need to display all this jazz, so every key really needs to be calculated and displayed.


*/

#include "mon.h"
#include "map.h"
#include "BreakNeck.h"
#include "playerMovement.h"
#include "paint.h"

#include <stdio.h>
#include <math.h> //cos sin

Motions_t g_keyMotions[10];




// So when the player runs 5 squares, all 5 steps get built up, and we resolve them here
//   Steps are a list of x/y locations.  Take one down, advance step
//   Return true if we need user input / we're out of array to process
//uh, big rolling array. 
//const int STEP_ARRAY_SIZE = 5;   //tsk, C is getting too smart for it's own good
#define STEP_ARRAY_SIZE 10
int g_playerSteps[STEP_ARRAY_SIZE][2]={0};  // [][0]=x, [][1] = y.
int g_currentStep = 0;
int g_quedStep = 0;
#//typedef enum relDir_t {FWD, FWD_R, FACE_RIGHT, FACE_BACK_R, FACE_BACK, FACE_BACK_L, FACE_LEFT, FWD_L} relDir_t;
int  oneStepThroughPlayerPath()
{
  if(g_currentStep == g_quedStep)
  {
    printf("End of playerSteps, seek input\n");
    return 1;
  }

  int x = g_playerSteps[g_currentStep][0];
  int y = g_playerSteps[g_currentStep][1];

  if( isPassable(x, y) || g_jumping )
  {
    if(inMap(x,y))
      monPlacePlayer( x, y);
  }
  else
  {
    printf("OOF!\n");
    consoleAdd("OOF!");
    g_mon[0].speed = SPEED_STILL;

  }

/*
  if(g_map[x][y].special == TILE_MONEY)
  {
    g_map[x][y].special = TILE_NOTHING;
    g_money += 1;
  }
*/

  g_currentStep = ( g_currentStep + 1) % STEP_ARRAY_SIZE;
  return 0;
}

void appendPlayerPath(int x, int y)
{
  printf("appending %d, %d\n", x,y);
  g_playerSteps[g_quedStep ][0] = x; 
  g_playerSteps[g_quedStep ][1] = y; 
  g_quedStep = ( g_quedStep + 1) % STEP_ARRAY_SIZE;
}

//Just duplicating Bresenham again, whatever
//   ...I COULD do the whole function pointer thing to do something along the path... 
int makePlayerPathFromHereToThere(int x1, int y1, int x2, int y2)
{
  int dx, dy, i, e;
  int incx, incy, inc1, inc2;
  int x,y;

  if(!inMap(x1,y1) || !inMap(x2,y2))
    return 0;

  dx = x2 - x1;
  dy = y2 - y1;

  if(dx < 0) { dx = -dx; }
  if(dy < 0) { dy = -dy; }
  incx = 1;
  if(x2 < x1) { incx = -1; }
  incy = 1;
  if(y2 < y1) { incy = -1; }
  x=x1;
  y=y1;

  if(dx > dy)
  {
    e = 2*dy - dx;
    inc1 = 2*( dy -dx);
    inc2 = 2*dy;
    for(i = 0; i <= dx-1; i++)   //TWEEEK! <=   need at least one step
    {
      if(e >= 0)
      {
        y += incy;
        e += inc1;
      }
      else { e += inc2; }
      x += incx;

      
      appendPlayerPath(x, y);
    }
  }
  else
  {
    e = 2*dx - dy;
    inc1 = 2*( dx - dy);
    inc2 = 2*dx;
    for(i = 0; i <= dy-1; i++)
    {
      if(e >= 0)
      {
        x += incx;
        e += inc1;
      }
      else { e += inc2; }
      y += incy;
      appendPlayerPath(x, y);
    }
  }
  return 1;
}









//God, thsi is annoying
const char* getRelStr(int relativeDirection)
{
  switch(relativeDirection)
  {
    case FACE_FWD: return "FACE_FWD";
    case FACE_FWD_R: return "FACE_FWD_R";
    case FACE_RIGHT: return "FACE_RIGHT";
    case FACE_BACK_R: return "FACE_BACK_R";
    case FACE_BACK: return "FACE_BACK";
    case FACE_BACK_L: return "FACE_BACK_L";
    case FACE_LEFT: return "FACE_LEFT";
    case FACE_FWD_L: return "FACE_FWD_L";
    case FACE_COAST: return "FACE_COAST";
    case FACE_JUMP:  return "JUMPING!";
  }
  return "wtf";
}


//Gimme a numpad key and a facing, I tell you the relative direciton
//relDir_t getRelativeDirection(int key, int facing )
int getRelativeDirection(int key, int facing )
{
  switch(key)
  {
    case '1': return (8+SOUTHWEST - facing) % NUM_DIRECTIONS;
    case '2': return (8+SOUTH     - facing) % NUM_DIRECTIONS;
    case '3': return (8+SOUTHEAST - facing) % NUM_DIRECTIONS;
    case '4': return (8+WEST      - facing) % NUM_DIRECTIONS;
    case '5': break; // FACE_COAST
    case '6': return (8+EAST      - facing) % NUM_DIRECTIONS;
    case '7': return (8+NORTHWEST - facing) % NUM_DIRECTIONS;
    case '8': return (8+NORTH     - facing) % NUM_DIRECTIONS;
    case '9': return (8+NORTHEAST - facing) % NUM_DIRECTIONS;
    case '0':
    case 'j':
    case 'J':
      g_jumping = 1; //GOOD A PLACE AS ANY!
      return FACE_JUMP;
      //return facing;
      break;
    default:
      printf("WTF kind of key is that?\n");
  }
  return FACE_COAST;
}


/*  otherwise known as bod-standard grid movement 
  \ ^ /
  < @ >
  / V \
*/ 
void actionStill(int cmd, int* fwd_o, int* left_o, int* newRelFacing_o, int* newSpeed_o)
{
  if(cmd != FACE_COAST) 
    *newRelFacing_o = cmd;
  *newSpeed_o = SPEED_WALK;
  switch(cmd)
  {
    case FACE_FWD    : 
      *fwd_o  = 1;
      *left_o = 0;
      break;
    case FACE_FWD_R   :
      *fwd_o  = 1;
      *left_o = -1;
      break;
    case FACE_RIGHT   :
      *fwd_o  = 0;
      *left_o = -1;
      break;
    case FACE_BACK_R  :
      *fwd_o  = -1;
      *left_o = -1;
      break;
    case FACE_BACK    :
      *fwd_o  = -1;
      *left_o = 0;
      break;
    case FACE_BACK_L  :
      *fwd_o  = -1;
      *left_o = 1;
      break;
    case FACE_LEFT    :
      *fwd_o  = 0;
      *left_o = 1;
      break;
    case FACE_FWD_L   :
      *fwd_o  = 1;
      *left_o = 1;
      break;
    case FACE_COAST   :
    case FACE_JUMP :
      *fwd_o  = 0;
      *left_o = 0;
      *newSpeed_o = SPEED_STILL;
      break;
  }
}





/*  
  ^ ^ ^
  \ ^ /
  < @ >
*/ 
void actionWalk(int cmd, int* fwd_o, int* left_o, int* newRelFacing_o, int* newSpeed_o)
{
  switch(cmd)
  {
    case FACE_FWD    : 
      *fwd_o  = 2;
      *left_o = 0;
      *newSpeed_o = SPEED_JOG;
      break;
    case FACE_FWD_R   :
      *fwd_o  = 2;
      *left_o = -1;
      break;
    case FACE_RIGHT   :
      *fwd_o  = 1;
      *left_o = -1;
      *newRelFacing_o = FACE_FWD_R;
      break;
    case FACE_BACK_R  :
      *fwd_o  = 0;
      *left_o = -1;
      *newRelFacing_o = FACE_RIGHT;
      break;
    case FACE_BACK    :
      *fwd_o  = 0;
      *left_o = 0;
      *newSpeed_o = SPEED_STILL;
      break;
    case FACE_BACK_L  :
      *fwd_o  = 0;
      *left_o = 1;
      *newRelFacing_o = FACE_LEFT;
      break;
    case FACE_LEFT    :
      *fwd_o  = 1;
      *left_o = 1;
      *newRelFacing_o = FACE_FWD_L;
      break;
    case FACE_FWD_L   :
      *fwd_o  = 2;
      *left_o = 1;
      break;
    case FACE_COAST   :
      *fwd_o  = 1;
      *left_o = 0;
      break;
    case FACE_JUMP :
      *fwd_o  = 2;
      *left_o = 0;
      *newSpeed_o = SPEED_STILL;
      break;

  }
}

/*  
    ^      8   R
  ^ ^ ^   759 JJJ
  \ ^ /   426 WWW    uh, but 4&6 double up with 1&2. But with diff dir and speed
    @      @   @
*/ 
void actionJog(int cmd, int* fwd_o, int* left_o, int* newRelFacing_o, int* newSpeed_o)
{
  switch(cmd)
  {
    case FACE_FWD    : 
      *fwd_o  = 3;
      *left_o = 0;
      *newSpeed_o = SPEED_RUN;
      break;
    case FACE_FWD_R   :
      *fwd_o  = 2;
      *left_o = -1;
      break;
    case FACE_RIGHT   :
      *fwd_o  = 1;
      *left_o = -1;
      break;
    case FACE_BACK_R  :
      *fwd_o  = 1;
      *left_o = -1;
      *newRelFacing_o = FACE_FWD_R;
      *newSpeed_o = SPEED_WALK;
      break;
    case FACE_BACK    :
      *fwd_o  = 1;
      *left_o = 0;
      *newSpeed_o = SPEED_WALK;
      break;
    case FACE_BACK_L  :
      *fwd_o  = 1;
      *left_o = 1;
      *newRelFacing_o = FACE_FWD_L;
      *newSpeed_o = SPEED_WALK;
      break;
    case FACE_LEFT    :
      *fwd_o  = 1;
      *left_o = 1;
      break;
    case FACE_FWD_L   :
      *fwd_o  = 2;
      *left_o = 1;
      break;
    case FACE_COAST   :
      *fwd_o  = 2;
      *left_o = 0;
      break;
    case FACE_JUMP :
      *fwd_o  = 4;
      *left_o = 0;
      *newSpeed_o = SPEED_WALK;
      break;

  }
}

/*  
    ^      8   B
    ^      5   R
  \ ^ /   729 RJR   but 4&7 are identical and the same place as 1, which adjusts facing&speed

    @      @   @
*/ 
void actionRun(int cmd, int* fwd_o, int* left_o, int* newRelFacing_o, int* newSpeed_o)
{
  switch(cmd)
  {
    case FACE_FWD    : 
      *fwd_o  = 4;
      *left_o = 0;
      *newSpeed_o = SPEED_BREAKNECK;
      break;
    case FACE_FWD_R   :
      *fwd_o  = 2;
      *left_o = -1;
      break;
    case FACE_RIGHT   :
      *fwd_o  = 2;
      *left_o = -1;
      break;
    case FACE_BACK_R  :
      *fwd_o  = 2;
      *left_o = -1;
      *newRelFacing_o = FACE_FWD_R;
      *newSpeed_o = SPEED_JOG;
      break;
    case FACE_BACK    :
      *fwd_o  = 2;
      *left_o = 0;
      *newSpeed_o = SPEED_JOG;
      break;
    case FACE_BACK_L  :
      *fwd_o  = 2;
      *left_o = 1;
      *newRelFacing_o = FACE_FWD_L;
      *newSpeed_o = SPEED_JOG;
      break;
    case FACE_LEFT    :
      *fwd_o  = 2;
      *left_o = 1;
      break;
    case FACE_FWD_L   :
      *fwd_o  = 2;
      *left_o = 1;
      break;
    case FACE_COAST   :
      *fwd_o  = 3;
      *left_o = 0;
      break;
    case FACE_JUMP :
      *fwd_o  = 6;
      *left_o = 0;
      *newSpeed_o = SPEED_JOG;
      break;

  }
}



/*  
    ^      8   B
    ^      *   R   
               

    @      @   @
*/ 
void actionBreakNeck(int cmd, int* fwd_o, int* left_o, int* newRelFacing_o, int* newSpeed_o)
{
  *newSpeed_o = SPEED_RUN;

  switch(cmd)
  {
    case FACE_FWD    : 
      *fwd_o  = 4;
      *left_o = 0;
      *newSpeed_o = SPEED_BREAKNECK;
      break;
    case FACE_JUMP :
      *fwd_o  = 8;
      *left_o = 0;
      *newSpeed_o = SPEED_RUN;
      break;
    default:
      *fwd_o  = 3;
      *left_o = 0;
  }
}


// put in FACE_FWD, jog  and get  3FWD 0Left
void actionDestLookup( int cmd, int speed, int* fwd_o, int* left_o, int* newRelFacing_o, int* newSpeed_o)
{
  switch(speed)
  {
    case SPEED_STILL : actionStill(cmd, fwd_o, left_o, newRelFacing_o, newSpeed_o);   
      break;
    case SPEED_WALK :  actionWalk(cmd, fwd_o, left_o, newRelFacing_o, newSpeed_o);   
      break;
    case SPEED_JOG :  actionJog(cmd, fwd_o, left_o, newRelFacing_o, newSpeed_o);   
      break;
    case SPEED_RUN : actionRun(cmd, fwd_o, left_o, newRelFacing_o, newSpeed_o);   
      break;
    case SPEED_BREAKNECK : actionBreakNeck(cmd, fwd_o, left_o, newRelFacing_o, newSpeed_o);   
      break;
    default:
      printf("wtf oh god THE SPEEED\n");
  }

}

// We're going with North as the 0 point standard. 
//0 = north = no rotation.    only n, ne, e, se, etc.
//... uuuuh, and it's regular sane grid coordinates here, no curses inverted y yet.
void gridRotation( int inX, int inY, int* outX, int* outY, int rotateByFacing)
{
  double x = inX ;
  double y = inY ;
  //double deg = rotateByFacing * 0.785398; //45 deg   1.5708
  double deg = rotateByFacing * 45  *3.14159 / 180.0;
  double newX = cos(deg) * x + sin(deg) * y;  //Fuck it, MATH
  double newY = -sin(deg) * x + cos(deg) * y;
  //printf("(pretending we going north)--in:%d,%d rad:%2.2f cos(deg)=%2f sin(deg)=%2f\n",inX, inY, deg, cos(deg), sin(deg));  
  *outX = round(newX);
  *outY = round(newY);
}

//0 = north = Forward.    only n, ne, e, se, etc.
//   So if we're facing NE, and the input direction is North, it returns FWD_LEFT (or NW)
// Likewise, If we're facing E and we want to rotate FWD, it returns E
//           If we're facing W and we want to rotate FACE_LEFT, it return S
int roateByFacing(int inputDirection, int rotateByFacing)
{
  if( inputDirection == FACE_COAST) return FACE_COAST;
  return (NUM_DIRECTIONS+inputDirection + rotateByFacing) % NUM_DIRECTIONS;
}


///Moves the player according to their keyboard input
int playerMovementInput(int key)
{
  //TODO: You didn't REALLY want to duplicate this, you just never got around to making use of the calculated values in place of this code. 

  int ret = 0;
  int x = g_mon[MON_PLAYER].x;
  int y = g_mon[MON_PLAYER].y;
  int facing = g_mon[MON_PLAYER].facing;
  int speed  = g_mon[MON_PLAYER].speed;

  //Relative directions.
  int inputRelativeDirection; // FWD, LEFT, FACE_BACK_L;
  //Relative destination 
  int fwd;
  int left;
  // Where do we go from here?
  int destX, destY;

/* new plan,  lookup and rotation
1 - take input 1-9  ->  relative direction
2 - destiationLookup(relative dir, speed)   ->  relative destination,  2F 1L   
3 - speedChangeLookup(relative dir, speed)  ->  new speed
4 - facingChangeLookup(relative dir, speed) ->  new facing
  +  oh yeah, turning fwd into x/y coordinats based on our facing.   "Rotation". 
5 - Make path from here to there   ->  vector of positions
6 - (outback in main loop) path resolves,  we animate and check for collisions
*/
  //Put in '6', get "fwd"
  inputRelativeDirection = getRelativeDirection(key, facing );
  printf("Facing: %d. cmd numpad:'%c', which is rel: %s\n", facing, key, 
      getRelStr(inputRelativeDirection));
  // put in jog, Fwd,   and get  3FWD 0Left
  int turnDirection = FACE_FWD;
  int newSpeed = speed;
  actionDestLookup(inputRelativeDirection, speed, &fwd, &left, &turnDirection, &newSpeed);
  printf("realtive movement, FWD: %d LEFT: %d\n", fwd, left);
  // We can assume FWD is north and rotate it around by our facing
  gridRotation(-left, fwd, &destX, &destY, facing);
  printf("realtive destination: +X: %d  (-)Y: %d  (and turning %s [newspeed %d])\n", destX, destY, 
      getRelStr(turnDirection), newSpeed);
  destX = destX + x;
  destY = -destY + y;  //omg, curses is just backwards
  printf("CurLoc: X%d  Y%d ->  TargetLoc X%d Y%d\n", x, y, destX, destY);

  //Make a path from here to there
  makePlayerPathFromHereToThere(x, y, destX, destY);
  //monPlacePlayer(destX, destY);
  g_mon[0].facing = roateByFacing(facing, turnDirection);
  g_mon[0].speed = newSpeed;

  

  //Debugging
  sprintf(g_bugName[0],"speed"); g_bug[0] = speed;
  sprintf(g_bugName[1],"face "); g_bug[1] = facing;



  return ret;
}

void initMotionKeys()
{
  g_keyMotions[0].keyChar = 'J';
  g_keyMotions[1].keyChar = '1';
  g_keyMotions[2].keyChar = '2';
  g_keyMotions[3].keyChar = '3';
  g_keyMotions[4].keyChar = '4';
  g_keyMotions[5].keyChar = '5';
  g_keyMotions[6].keyChar = '6';
  g_keyMotions[7].keyChar = '7';
  g_keyMotions[8].keyChar = '8';
  g_keyMotions[9].keyChar = '9';
}


//Build up content of keyMotions[9], for displaying
void playerPotentialMovement()
{
  int x = g_mon[MON_PLAYER].x;
  int y = g_mon[MON_PLAYER].y;
  int facing = g_mon[MON_PLAYER].facing;
  int speed  = g_mon[MON_PLAYER].speed;
//  int speed  = 1;   // debugging, keep it in sync,  TODO remove

  //Relative directions.
  int inputRelativeDirection; // FWD, LEFT, FACE_BACK_L;
                              //Relative destination 
  int fwd;
  int left;
  //Transitions
  // Where do we go from here?
  int destX, destY;

  // j= 0,  1-9 are numpad
  for(int key = 0; key<=9; key++)
  {
    //Put in '6', get "fwd"
    inputRelativeDirection = getRelativeDirection(g_keyMotions[key].keyChar, facing );

    //printf("Facing: %d. key %d which is rel: %s\n", facing, key, getRelStr(inputRelativeDirection));
    // put in jog, Fwd,   and get  3FWD 0Left
    int turnDirection = FACE_FWD;
    int newSpeed = speed;
    actionDestLookup(inputRelativeDirection, speed, &fwd, &left, &turnDirection, &newSpeed);
    //printf("realtive movement, FWD: %d LEFT: %d\n", fwd, left);
    // We can assume FWD is north and rotate it around by our facing
    gridRotation(-left, fwd, &destX, &destY, facing);
    turnDirection = roateByFacing(facing, turnDirection);
    //printf("realtive destination: +X: %d  +Y: %d  (and now facing %d)\n", destX, destY, turnDirection);
    destX = destX + x;
    destY = -destY + y;  //omg, curses is just backwards
    //printf("CurLoc: X%d  Y%d ->  TargetLoc X%d Y%d\n", x, y, destX, destY);
    // DEBUG printf("%d -> X%d Y%d\n", key, destX, destY);

    g_keyMotions[key].destX = destX;
    g_keyMotions[key].destY = destY;
    g_keyMotions[key].newFacing = turnDirection;
    g_keyMotions[key].newSpeed = newSpeed;
  }
}



