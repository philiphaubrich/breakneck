/** genMap.c
In charge of generating new maps to play in. 
That's it. But the functions are so bulky, they deserved to live somewhere else.
*/

#include "genMap.h"
#include "map.h"
#include "mon.h"
#include "BreakNeck.h"
#include "libtcod/libtcod.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define UP 0
#define RIGHT 1
#define DOWN 2
#define LEFT 3



int g_goalX;  //Just so we can shuffle it from one function to another. Yeah, I'm a lazy fucker
int g_goalY;




///It's sad these are needed, but I honestly find my self fucking up simple stuff. 
void genMapSetWall(int x, int y)
{
  if( !inMap(x,y))
    return;
  g_map[x][y].isWall = 1;
  TCOD_map_set_properties(g_tcodMap, x, y, 0,0);
}

void genMapClearWall(int x, int y)
{
  g_map[x][y].isWall = 0;
  TCOD_map_set_properties(g_tcodMap, x, y, 1,1);
}

void generateOrthagonalWalls(int x, int y, int dirX, int dirY, int length)
{
  for( int i=0; i<length; i++)
  {
    x += dirX;
    y += dirY;
    if(inMap(x,y))
      genMapSetWall(x,y);
  }
}
/// Bug-hunting function. At one point I found my maps corrupted by a buffer overflow
/// Used to initialize g_numWalls as well as to validate non disappeared.
///@return a count of the number of wall tiles found in a map
int g_numWalls;
int validateWalls()
{
  int x,y, count=0;
  for(x=0; x<MAP_SIZE; x++)
  {
    for(y=0; y<MAP_SIZE; y++)
    {
      if(g_map[x][y].isWall)
      {
        count++;
      }
    }
  }
  return count;
}

///Let's verify the map is solvable.
///@return 0 = invalid,  1 = valid
int isMapValid()
{

  TCOD_path_t path = TCOD_path_new_using_map(g_tcodMap, 0.0f); 
  int dist;

//OOf, ok, fine, it's just for tower maps.
//  TCOD_path_compute( path, g_mon[MON_PLAYER].x, g_mon[MON_PLAYER].y, g_goalX, g_goalY);
  TCOD_path_compute( path, MAP_SIZE/2-1, MAP_SIZE/2-1, MAP_SIZE/2+1, MAP_SIZE/2+1);
  dist = TCOD_path_size(path); 
  printf("mapDist = %d\n", dist);
  if(dist <= 0)
  {
    return 0;
  }
  return 1;
}

///Place an exit somewhere on the right-side of the map
///@return 0 = normal,  -1 = insane, need to exit, GTFO
int genMapExit()
{
  int x,y;
  int sanity=1000;
  do
  {
    x = MAP_SIZE-1;
    y = rand()%MAP_SIZE;
    if(!sanity--) {printf("SanLoss\n");return -1;}
  }while(g_map[x][y].isWall);
  g_map[x][y].special = TILE_UPSTAIR;
  g_goalX = x;
  g_goalY = y;
  //TODO should probably verify that the player has a path to the exit. You know, to make sure it's a winnable puzzle.  isMapSolvable();   yeaaaah. 
  //bool TCOD_map_is_walkable(TCOD_map_t map, int x, int y)
  return 0;
}


///Starts with an empty map. Called by more interesting map generators
void generateEmptyMap()
{
  int x,y;

  g_tcodMap = TCOD_map_new(MAP_SIZE, MAP_SIZE);
  g_tcodMapMonView = TCOD_map_new(MAP_SIZE,MAP_SIZE);


  TCOD_map_clear(g_tcodMap, 1, 1);

  for(x=0; x<MAP_SIZE; x++)
  {
    for(y=0; y<MAP_SIZE; y++)
    {
      g_map[x][y].isWall = 0;
      g_map[x][y].special = TILE_NOTHING;
      g_map[x][y].mon = 0;
    }
  }
}

///Makes square room with chance of an opening on each wall
void genMapBuilding(int x1, int y1, int x2, int y2)
{
  int x,y;
  //printf("box: %d,%d  -> %d,%d\n",x1,y1,x2,y2);

  x1 = MIN(x1, MAP_SIZE-1);
  x1 = MAX(x1, 0);
  y1 = MIN(y1, MAP_SIZE-1);
  y1 = MAX(y1, 0);
  x2 = MIN(x2, MAP_SIZE-1);
  x2 = MAX(x2, 0);
  y2 = MIN(y2, MAP_SIZE-1);

  if(x2-x1 < 3 || y2 - y1 < 3)
    return;


  for(x=x1; x<=x2 && x<MAP_SIZE; x++)
  {
    genMapSetWall(x, y1);
    genMapSetWall(x, y2);
  }
  for(y=y1; y<=y2 && y<MAP_SIZE; y++)
  {
    genMapSetWall(x1, y);
    genMapSetWall(x2, y);
  }

  //Chance of door on each wall, but not the corners
  if(rand()%2 == 0)
  {
    x = x1+rand()%(x2-x1-2)+1;
    y = y1;
    genMapClearWall(x, y);
  }
  if(rand()%2 == 0) 
  {
    x = x1+rand()%(x2-x1-2)+1;
    y = y2;
    genMapClearWall(x, y);
  }
  if(rand()%2 == 0) 
  {
    x = x1;
    y = y1+rand()%(y2-y1-2)+1;
    genMapClearWall(x, y);
  }
  if(rand()%2 == 0)
  {
    x = x2;
    y = y1+rand()%(y2-y1-2)+1;
    genMapClearWall(x, y);
  }
}


void genMapFillSquare(int x1, int y1, int x2, int y2)
{
  //God, so lame, but I'm DOWN to the wire.   
  for(int x=x1; x<=x2 && x<MAP_SIZE; x++)
  {
    for(int y=y1; y<=y2 && y<MAP_SIZE; y++)
    {
      genMapSetWall(x, y);
    }
  }
   
}


///Makes square, off-screen just gets dropped
//   -1 wall thickness means fill in the whole thing.
void genMapSquare(int x1, int y1, int x2, int y2, int thickness)
{
  int x,y;
  //printf("box: %d,%d  -> %d,%d\n",x1,y1,x2,y2);

  if(thickness == -1)
  {
    genMapFillSquare(x1,y1,x2,y2);
    return;
  }

  for(int i=0; i<thickness; i++)
  {
    x1++;
    y1++;
    x2--;
    y1--;
    for(x=x1; x<=x2 && x<MAP_SIZE; x++)
    {
      genMapSetWall(x, y1);
      genMapSetWall(x, y2);
    }
    for(y=y1; y<=y2 && y<MAP_SIZE; y++)
    {
      genMapSetWall(x1, y);
      genMapSetWall(x2, y);
    }
  }
}



void printCov(int cov[MAP_SIZE][MAP_SIZE])
{
  for(int x=0; x<MAP_SIZE; x++)
  {
    for(int y=0; y<MAP_SIZE; y++)
    {
      printf("%c",cov[x][y]?' ':'X');
    }
    printf("\n");
  }
   printf("\n\n");
}

#define ROOM_SIZE 5
#define ROOM_MIN  4


//Ok, try to make a room growing from x,y  in a direction,   NOT hitting any old point in the coverage map
//  recursively call this for all 4 sides.   simply return if you can't make a room
void genMapWallWalk(int x, int y, int cov[MAP_SIZE][MAP_SIZE], int direction)
{
  //printCov(cov); That was really just for debugging
  int x1,x2, y1,y2;
  switch(direction)
  {
    case UP:
      if(rand()%2)
      {
        x1 = x;
        x2 = x + rand()%ROOM_SIZE + ROOM_MIN;
      }
      else
      {
        x2 = x;
        x1 = x - rand()%ROOM_SIZE - ROOM_MIN;
      }
      y1 = y - rand()%ROOM_SIZE - ROOM_MIN;
      y2 = y; 
      break;
    case RIGHT:  //So there's probably some way of generalizing this and just passing in the direction, but I can't think og it
      if(rand()%2)
      {
        y1 = y;
        y2 = y + rand()%ROOM_SIZE + ROOM_MIN;
      }
      else
      {
        y2 = y;
        y1 = y - rand()%ROOM_SIZE - ROOM_MIN;
      }
      x1 = x; 
      x2 = x + rand()%ROOM_SIZE + ROOM_MIN; 
      break;
    case DOWN:
      if(rand()%2)
      {
        x1 = x;
        x2 = x + rand()%ROOM_SIZE + ROOM_MIN;
      }
      else
      {
        x2 = x;
        x1 = x - rand()%ROOM_SIZE - ROOM_MIN;
      }
      y1 = y;
      y2 = y + rand()%ROOM_SIZE + ROOM_MIN; 
      break;
    case LEFT:
      if(rand()%2)
      {
        y1 = y;
        y2 = y + rand()%ROOM_SIZE + ROOM_MIN;
      }
      else
      {
        y2 = y;
        y1 = y - rand()%ROOM_SIZE - ROOM_MIN;
      }
      x1 = x - rand()%ROOM_SIZE - ROOM_MIN;
      x2 = x; 
      break;  
  }

  //Throw out all the bad shit
  if(x1 < 0 || x2 >=MAP_SIZE || y1 < 0 || y2 >= MAP_SIZE)
    return;

  for(int testx = x1; testx < x2; testx++)
  {
    if(cov[testx][y1] == 1)
      return;
    if(cov[testx][y2] == 1)
      return;
  }
  for(int testy = y1; testy < y2; testy++)
  {
    if(cov[x1][testy] == 1)
      return;
    if(cov[x2][testy] == 1)
      return;
  }

  //Actually make the room
  for(int tmpx = x1; tmpx < x2; tmpx++)
  {
    genMapSetWall(tmpx, y1);
    genMapSetWall(tmpx, y2);
  }
  for(int tmpy = y1; tmpy <= y2; tmpy++)  //Was missing bottom right corner
  {
    genMapSetWall(x1, tmpy);
    genMapSetWall(x2, tmpy);
  }

  //X and y are fair game
  //Chance of door on each wall, but not the corners
  if(rand()%2 == 0)
  {
    x = x1+rand()%(x2-x1-2)+1;
    y = y1;
    genMapClearWall(x, y);
  }
  if(rand()%2 == 0) 
  {
    x = x1+rand()%(x2-x1-2)+1;
    y = y2;
    genMapClearWall(x, y);
  }
  if(rand()%2 == 0) 
  {
    x = x1;
    y = y1+rand()%(y2-y1-2)+1;
    genMapClearWall(x, y);
  }
  if(rand()%2 == 0)
  {
    x = x2;
    y = y1+rand()%(y2-y1-2)+1;
    genMapClearWall(x, y);
  }


  //Might need random other doors too


  for(int tmpx = x1+1; tmpx < x2-1; tmpx++)
  {
    cov[tmpx][y1+1] = 1;
    cov[tmpx][y2-1] = 1;
  }

  for(int tmpy = y1+1; tmpy < y2-1; tmpy++)
  {
    cov[x1+1][tmpy] = 1;
    cov[x2-1][tmpy] = 1;
  }

  int try[4]= {1,1,1,1};
  while(try[0] || try[1] || try[2] || try[3])
  {
    switch( rand()%4 )
    {
      case UP: 
        if(!try[UP]) continue;
        try[UP] = 0;
        genMapWallWalk( x1+1+rand()%(x2-x1) ,y1, cov, UP);   //oh god.   
        break;
      case RIGHT: 
        if(!try[RIGHT]) continue;
        try[RIGHT] = 0;
        genMapWallWalk( x2, y1+1+rand()%(y2-y1), cov, RIGHT); 
        break;
      case DOWN: 
        if(!try[DOWN]) continue;
        try[DOWN] = 0;
        genMapWallWalk( x1+1+rand()%(x2-x1) ,y2, cov, DOWN);
        break;
      case LEFT: 
        if(!try[LEFT]) continue;
        try[LEFT] = 0;
        genMapWallWalk( x1, y1+1+rand()%(y2-y1), cov, LEFT); 
        break;
    }
  }
}



///Generate a typical Tower floor
/// The tower is square now
///  Exterior wall
///  centerline down the middle, N/S or E/W
int generateTowerMap()
{
  int san=1000;
  do
  {
    generateEmptyMap();
    int centerX = MAP_SIZE/2;
    int centerY = MAP_SIZE/2;
    int x,y;
    //int i;
    //int x1,y1,x2,y2;
    //int numBoxes, boxSize, length;
    //int dir;

    //Form border walls
    for(x=0; x<MAP_SIZE; x++)
    {
      y=0;
      genMapSetWall(x, y);
      y=MAP_SIZE-1;
      genMapSetWall(x, y);
    }
    for(y=0; y<MAP_SIZE; y++)
    {
      x=0;
      genMapSetWall(x, y);  
      x=MAP_SIZE-1;
      genMapSetWall(x, y);   
    }

    // Generate center wall
    int isNS = rand()%2;
    if( isNS ) //NS or EW
    {
      for(x=5; x<MAP_SIZE-5; x++)
      {
        y = centerY;
        genMapSetWall(x, y);  
        y=MAP_SIZE-1;
        genMapSetWall(x, y);   
      }
    }
    else
    {
      for(y=5; y<MAP_SIZE-5; y++)
      {
        x = centerX;
        genMapSetWall(x, y);  
        x=MAP_SIZE-1;
        genMapSetWall(x, y);   
      }
    }

    //Make stairs on either side.
    g_map[centerX-1][centerY-1].special = TILE_DOWNSTAIR;
    g_map[centerX+1][centerY+1].special = TILE_UPSTAIR;
    g_goalX = centerX+1;
    g_goalY = centerY+1;  //huh, it's stationary now

   

    


    // ok new plan,   one hour left to go. 
    //    make grid and mark off sections. Fuck checking for walls
    //   build a room off another room. Random walk. backtrack if you can't walk anymore. knock holes as you go.

    // build a room on a side of the center wall
    for(x=centerX-5; x<centerX+5; x++)
    {
      y = centerY - 5;
      genMapSetWall(x, y);  
      y = centerY +5;
      genMapSetWall(x, y);   
    }
    for(y=centerY-5; y<centerY+5; y++)
    {
      x = centerX - 5;
      genMapSetWall(x, y);  
      x = centerX + 5;
      genMapSetWall(x, y);   
    }

    //Block out that center room
    int cov[MAP_SIZE][MAP_SIZE]; //Defaults to zeros, right?
    memset(cov, 0, sizeof(int)*MAP_SIZE*MAP_SIZE);
    for(x=centerX-4; x<centerX+4; x++)
    {
      cov[x][centerY+4] = 1;
      cov[x][centerY-4] = 1;
    }
    for(y=centerY-4; y<centerY+4; y++)
    {
      cov[centerX+4][y] = 1;
      cov[centerX-4][y] = 1;
    }
    //And the exterior walls
    for(x=0; x<MAP_SIZE; x++)
    {
      cov[x][0] = 1;
      cov[x][MAP_SIZE-1] = 1;
    }
    for(y=0; y<MAP_SIZE; y++)
    {
      cov[0][y] = 1;
      cov[MAP_SIZE-1][y] = 1;
    }


    //on a random side, pick a random point, and call the room-making randwalk,  do the same on the opposit point
    if(isNS)
    {
        genMapWallWalk(centerX-4+rand()%3, centerY-5, cov, UP);  
        genMapWallWalk(centerX-4+rand()%3, centerY+5, cov, DOWN);  
    }
    else
    {
        genMapWallWalk(centerX+5, centerY-4+rand()%3, cov, RIGHT);  
        genMapWallWalk(centerX-5, centerY-4+rand()%3, cov, LEFT);  
    }

    //Enforce some doors on the landings
    genMapClearWall(20,21);
    genMapClearWall(21,20);
    genMapClearWall(29,30);
    genMapClearWall(30,29);

    san--;
    if(!san)
    {
      printf("san loss mid map gen, bailing\n");
      return -2;
    }
  }
  while( !isMapValid());

  genMapAdditems();

  return 0;
}


///Generates a map with a major street intersection, buildings around the edges, doors, alleyways
///  Yeah, this is totally Mosybaia now
///@return 0 = normal,  non-0 = insane, need to exit, GTFO
int generateCityBlockMap()
{
  int san=1000;
  do
  {
    generateEmptyMap();

    int x,y;
    int x1,y1,x2,y2;
    //Random point near center of map
    //int centerX = rand()%MAP_SIZE/2 + MAP_SIZE/2;
    //int centerY = rand()%MAP_SIZE/2 + MAP_SIZE/2;
    //Or let's just keep it static so we know we have all 4 sides
    int centerX = MAP_SIZE/2;
    int centerY = MAP_SIZE/2;

    int streetWidth = 4;  //or 6 from the center spot
    int boxSize = 8;

    //Wow, this is a mess. verbose, piecemealed, and random so it's hard to tell what's broke. This is just plain bad code.   TODO, clean it up somehow. 




    //Starting at block corner, chance of buildings 
    //NW going N
    for(x=centerX-streetWidth, y=centerY-streetWidth; y>0; )
    {
      x2 = x - rand()%100/90; 
      y2 = y - (rand()%100/60)*2;
      x1 = x2 - rand()%(boxSize-4)-4;
      y1 = y2 - rand()%(boxSize-4)-4;
      y = y1;
      genMapBuilding(x1,y1,x2,y2);
    }
    //NW going W
    for(y=centerY-streetWidth, x=centerX-streetWidth; x>0; )
    {
      x2 = x - (rand()%100/60)*2;
      y2 = y - rand()%100/90;
      x1 = x2 - rand()%(boxSize-4)-4;
      y1 = y2 - rand()%(boxSize-4)-4;
      x = x1;
      genMapBuilding(x1,y1,x2,y2);
    }
    //SE going E
    for(y=centerY+streetWidth, x=centerX+streetWidth; x<MAP_SIZE; )
    {
      x1 = x + (rand()%100/60)*2;
      y1 = y + rand()%100/90;
      x2 = x1 + rand()%(boxSize-4)+4;
      y2 = y1 + rand()%(boxSize-4)+4;
      x = x2;
      genMapBuilding(x1,y1,x2,y2);
    }
    //SE going S
    for(y=centerY+streetWidth, x=centerX+streetWidth; y<MAP_SIZE; )
    {
      x1 = x + (rand()%100/60)*2;
      y1 = y + rand()%100/90;
      x2 = x1 + rand()%(boxSize-4)+4;
      y2 = y1 + rand()%(boxSize-4)+4;
      y = y2;
      genMapBuilding(x1,y1,x2,y2);
    }
    //SW going W
    for(y=centerY+streetWidth, x=centerX-streetWidth; x>0; )
    {
      x1 = x - rand()%(boxSize-4)-4;
      y1 = y + rand()%100/90;
      x2 = x + (rand()%100/60)*2;
      y2 = y1 + rand()%(boxSize-4)+4;
      x = x1;
      genMapBuilding(x1,y1,x2,y2);
    }
    //SW going S
    for(y=centerY+streetWidth, x=centerX-streetWidth; y<MAP_SIZE; )
    {
      x1 = x - rand()%(boxSize-4)-4;
      y1 = y + (rand()%100/60)*2;
      x2 = x + rand()%100/90;
      y2 = y1 + rand()%(boxSize-4)+4;
      y = y2;
      genMapBuilding(x1,y1,x2,y2);
    }  
    //NE going E
    for(y=centerY-streetWidth, x=centerX+streetWidth; x<MAP_SIZE; )
    {
      x1 = x + (rand()%100/60)*2;
      y1 = y - rand()%(boxSize-4)-4;
      x2 = x + rand()%(boxSize-4)+4;
      y2 = y + rand()%100/90;
      x = x2;
      genMapBuilding(x1,y1,x2,y2);
    }
    //NE going N
    for(y=centerY-streetWidth, x=centerX+streetWidth; y>0; )
    {
      x1 = x + rand()%100/90;
      y2 = y + (rand()%100/60)*2;
      x2 = x + rand()%(boxSize-4)+4;
      y1 = y2 - rand()%(boxSize-4)-4;
      y = y1; 
      genMapBuilding(x1,y1,x2,y2);
    }


    //And to make it worse, the players home is hard hardcoded and ugly
    for(x=0; x<10; x++)
    {
      for(y=0; y<10; y++)
      {
        genMapClearWall(x ,y);
      }
    }
    for(x=2; x<8; x++)
    {
      genMapSetWall(x, 8);
      genMapSetWall(x, 2);
    }
    for(y=2; y<8; y++)
    {
      genMapSetWall(8, y);
      genMapSetWall(2, y);
    }
    genMapClearWall(7, 8);

    //And place the stairs at the top
    g_map[centerX][0].special = TILE_UPSTAIR;
    g_goalX = centerX;
    g_goalY = 0;

    san--;
    if(!san)
    {
      printf("san loss mid map gen, bailing\n");
      return -2;
    }
  }
  while(!isMapValid());
  g_numWalls = validateWalls();
  return 0;
}

int generateSplitRooftopMap()
{
    //TODO,  but, like.  "A harder level". 
    return 0;
}

//TODO, experiment with East being the goal.  North being empty air. Raise the level fo the building (south wall) every level. 


///Generates a big roof-top. 
/// Street off to the left, nearby rootf-top (with exits) N, S, E
/// random junk
///  buildings
///  Tiered levels? 
///@return 0 = normal,  non-0 = insane, need to exit, GTFO
int generateBigRooftopMap(int level)
{
  int san=1000; //God, I have no idea how to test for validity here.
  //int centerX = MAP_SIZE/2;
  //int centerY = MAP_SIZE/2;

  int westStreetGap = 3;
  int northStreetGap = level+1;
  //int eastStreetGap = rand()%7;  //I dunno, some bonus level thing  
    //Actually, let them jump around on the HUD
  int eastStreetGap = 5;

  //do
  {
    generateEmptyMap();

    int x,y;
    int x1,y1,x2,y2;
    int boxSize = 8;

    //Wall around the border as the "big building". 
    genMapSquare(westStreetGap,northStreetGap,MAP_SIZE-eastStreetGap,MAP_SIZE-1,1);
    //TODO: everything outside of that needs to be open-air = insta-death!

    //TODO: Notches in the big building

    //North building, "the goal"   
    genMapSquare(westStreetGap, -1, MAP_SIZE-eastStreetGap, 1, 1);

    for(int numBuilding=rand()%10+4; numBuilding>0; numBuilding--)
    {
      x = rand()%(MAP_SIZE - 7) + 7;
      y = rand()%(MAP_SIZE - 7) + 7;
      x2 = x - rand()%100/90; 
      y2 = y - (rand()%100/60)*2;
      x1 = x2 - rand()%(boxSize-4)-4;
      y1 = y2 - rand()%(boxSize-4)-4;
      y = y1;
      switch(rand()%3)
      {
        case 0:
          genMapBuilding(x1,y1,x2,y2);
          break;
        case 1:
          genMapSquare(x1,y1,x2,y2,rand()%3+1);
          break;
        case 2:
          genMapSquare(x1,y1,x2,y2,-1);
      }
    }

    // some random walls
    for(int randWalls=rand()%10+5*level; randWalls>0; randWalls--)
    {
      generateOrthagonalWalls(rand()%MAP_SIZE, rand()%MAP_SIZE, 
                        rand()%3-1, rand()%3-1, rand()%10);
    }

    //And place the stairs at diminishing sections on the north building
    for(int x=westStreetGap; x<MAP_SIZE-eastStreetGap; x++)
    {
      if(x%(level+1) < 3)
        g_map[x][0].special = TILE_UPSTAIR;
      else
      {
        g_map[x][0].isWall = true;
        g_map[x][0].special = TILE_NOTHING;
      }
    }

    //Clear area around player's SPAWN!
    //    monPlacePlayer(MAP_SIZE/2,MAP_SIZE-2);
    for(int x=MAP_SIZE/2-5; x<MAP_SIZE/2+5; x++)
    {
      for(int y=MAP_SIZE-5; y<MAP_SIZE-1; y++)
      {
        genMapClearWall(x,y);
      }
    }

    san--;
    if(!san)
    {
      printf("san loss mid map gen, bailing\n");
      return -2;
    }
  }
  //while(!isMapValid());
  g_numWalls = validateWalls();
  return 0;
}


void genMapAdditems()
{
  int numMoney = 40;
  int x,y;
   
  for(int i=0; i<numMoney; i++)
  {
    x = rand()%MAP_SIZE;  
    y = rand()%MAP_SIZE;  
    if(!g_map[x][y].isWall && g_map[x][y].special == TILE_NOTHING )
      g_map[x][y].special = TILE_MONEY;
  }
}


