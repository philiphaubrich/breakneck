#So this is the fully automatic makefile that does ALLLLL sorts of stuff in the background under the hood.
# Specifically if it needs to make any .o file it assumes a default rule
# $(CC) $(CFLAGS) -c $@ $*.c
# $@ is the full target name
# $* is the target name with the suffix cut off, so they can add the .c

P       = BreakNeck
OBJECTS = ai_guard.o genMap.o map.o mon.o paint.o playerMovement.o BreakNeck.o
CFLAGS  = -g -Wall  -I./libtcod-develop/src
LIBS    = -lm
LDFLAGS = -ltcod
CC      = gcc
#CC      = c99

#$(P): $(OBJECTS) libtcod
#	$(CC) $@.c $(OBJECTS) -o $@ $(LDFLAGS) $(LIBS) $(CFLAGS) -Wl,-rpath,.

$(P): $(OBJECTS) libtcod
	$(CC) $(OBJECTS) -std=c11 -o $@ $(LDFLAGS) $(LIBS) $(CFLAGS) -std=gnu99 -Wl,-rpath,.


unit: $(P) unit$(P).o libtcod
	$(CC) $(P).c unit$(P).o -o unit$(P).exe -DTEST $(LDFLAGS) $(LIBS) $(CFLAGS)

unit$(P).o: unit$(P).c libtcod
	$(CC) unit$(P).c -c -DTEST $(LDFLAGS) $(LIBS) $(CFLAGS)

libtcod:
	cd ./libtcod-develop/buildsys/autotools/;	./configure;	make;	make install

debug: CFLAGS += -DDEBUG
debug: $(P)

clean: 
	rm -vf *.o
	rm -f $(P)
	rm -f unit$(P).exe
	rm -f tags


.PHONY: libtcod clean debug unit
