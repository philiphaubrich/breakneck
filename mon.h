#ifndef __BREAKNECK_MON__
#define __BREAKNECK_MON__

#include "BreakNeck.h"

//The players is a monster like the rest of them
#define MON_PLAYER 0 

#define MON_TYPE_PLAYER '@'
#define MON_TYPE_GUARD  'G'
#define MON_TYPE_NULL   '!'

typedef struct Monster_t
{
  int x;
  int y;
  int facing;
  int speed;
  char type;  //Char for curses, which icon to use

  char name[4];

  //int endurance;   // goes down for all actions, goes up with eating. 
  //int maxEndurance;  

 

  //AI stuff
  //int (*aiStateMachine)(void);  //Function pointer, woo. 
  int aiState; //Fuck it, just use switches for now.
  int aiPrevState;   //So we can go back to what we were doing. Kinda like what we'd like to do if we weren't interrupted

  int targetX; // where we're going.    ie, they have enough memory for ONE space. pft.
  int targetY;
   
}Monster_t;

extern Monster_t g_mon[MAX_MONSTERS]; 


//void monManageEndurance(int i, int action);

void monGenerateMonstersRandom(int numToMake);
void monInitPlayer();
void monProcessAI();

void monRemove(int i);
void monCorpseCart();
void monRemoveForRealzies(int i);

void monGenerate(int x, int y, int monType);
void monPlacePlayer(int x, int y);

int monMove(int i, int x, int y);

int monPickRandomTravelTarget(int i);

int monNearestTo(int x, int y, int g_statOffset);

void clearMonsters();

#endif
