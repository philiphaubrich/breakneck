/** mon.c
In charge of monsters, and executing their actions
*/

#include "mon.h"
#include "ai_guard.h"
#include "map.h"
#include "paint.h"
#include "BreakNeck.h"
#include "libtcod/libtcod.h"
//#include <stdio.h>
#include <stdlib.h>
#include <string.h> //Just for memset
#include <math.h>

int gNumMonsters = 1;   //Includes player, initially just the player
struct Monster_t g_mon[MAX_MONSTERS];


//Zeroes out data in g_mon
//Sets gNumMonsters to 1 (for the player)
void clearMonsters()
{
  int i;
  for(i=1; i< MAX_MONSTERS; i++)  //Alright, fine, the player isn't a monster.
  {
    memset(&g_mon[i], 0, sizeof(g_mon));
    g_mon[i].type = MON_TYPE_NULL;  //Char for curses
  }
  gNumMonsters = 1; //just theplayer?
}

/// Make some random dudes. placed randomly. Updates gNumMonsters
//TODO we could have this jump to type-specific stats held out in ai_herp.c
void monGenerateMonstersRandom(int numToMake)
{
  int x,y,i;

  unsigned int preservedSeed = rand();

  //Ok, so this has to change from clobbering all monsters to AMENDING additional mosters to the end

  for(i=gNumMonsters; i < gNumMonsters + numToMake && i < MAX_MONSTERS; i++) 
  {
    do
    {
      x = rand()%MAP_SIZE; 
      y = rand()%(MAP_SIZE/2);        ///Just top half for breakneck
    } while(!isPassable(x,y));
    g_map[x][y].mon = i;

    g_mon[i].x = x;
    g_mon[i].y = y;
    g_mon[i].facing = rand()%4;
    g_mon[i].speed = SPEED_WALK;
    g_mon[i].type = MON_TYPE_GUARD;  //Char for curses
    //All the fun shit
    g_mon[i].aiPrevState = AI_GUARD_WANDER;  
    g_mon[i].aiState = AI_GUARD_WANDER;

    monPickRandomTravelTarget(i);

    const char consenant[20] = "bcdfghjklmnpqrstvwxz";
    const char vowel[6] = "aeiouy"; 
    g_mon[i].name[0] = consenant[rand()%20];
    g_mon[i].name[1] = vowel[rand()%6];
    g_mon[i].name[2] = consenant[rand()%20];
    g_mon[i].name[3] = 0; 

    srand( g_mon[i].name[0]*1000 + g_mon[i].name[1]*100 + g_mon[i].name[2]);   //Whatever
  
  }
  srand(preservedSeed);
  gNumMonsters = i;
}

//Caller is responsible for updating g_map.mon
void monGenerate(int x, int y,  int monType)
{
  monGenerateMonstersRandom(1);
  int i = gNumMonsters -1;  //That'll be the newest one, right?
  
    g_mon[i].type = MON_TYPE_GUARD;
    g_mon[i].aiState = AI_GUARD_WANDER;
    g_map[g_mon[i].x][g_mon[i].y].mon = 0;
    g_mon[i].x = x;
    g_mon[i].y = y;
    g_map[x][y].mon = i;
}

///Sets up the player's values. Should only be called once at boot.
void monInitPlayer()
{
  g_mon[MON_PLAYER].type = MON_TYPE_PLAYER;

  /* moot, overwrited in stageSetup
  int x, y;
  do
  {
    x = 0;
    y = rand()%MAP_SIZE;
  } while(!isPassable(x,y));
  g_mon[MON_PLAYER].x = x;
  g_mon[MON_PLAYER].y = y;
  */
  g_mon[MON_PLAYER].facing = NORTH;
  g_mon[MON_PLAYER].speed = SPEED_STILL;
 
  sprintf(g_mon[MON_PLAYER].name, "you");
 
  //These aren't really used for the player. This is what I get for using a non-OO language. Overlap
  g_mon[MON_PLAYER].aiState = 0;
  g_mon[MON_PLAYER].targetX = 0;
  g_mon[MON_PLAYER].targetY = 0;
}

void monPlacePlayer(int x, int y)
{
  if(g_map[x][y].isWall)
  {
    consolePrint("You Jumped into a wall/void and died");
    // ...god it's too late.  Thsi is he last TODO  10:55pm of the 2022 7DRL challenge
  }

  g_mon[MON_PLAYER].x = x;
  g_mon[MON_PLAYER].y = y;
}

///Call the appropriate AI state.
void monProcessAI()
{
  int i;
  for(i=1; i < gNumMonsters && i < MAX_MONSTERS; i++) //Exclude player
  {  
    switch(g_mon[i].type)
    {
      case MON_TYPE_GUARD: aiGuard(i); break;
      default:
        wtf();
    }
  }
}

//Copy a monster in the g_mon array
void monCopy(int from, int to)
{
   memcpy ( &g_mon[to], &g_mon[from], sizeof(Monster_t) );
}


/*  dying guards is a TODO

///oooooooh shiiiiiit, all sorts of things assume the index is still legit after we do this. So we need to offload it to the end of the turn.  
//So this is now more like... SET to be removed. 
int g_corpses[MAX_MONSTERS]= {};
void monRemove(int i)
{
  g_corpses[i] = 1;
}


void monCorpseCart()
{
  int ughOffset=0;
  for(int i=1; i < gNumMonsters && i < MAX_MONSTERS; i++) //Exclude player
  {
    if(g_corpses[i+ughOffset] == 1)
    {
      monRemoveForRealzies(i);
      ughOffset++;
      i--; //OH GOD WHY
    }
  }
  memset(g_corpses, 0, sizeof(int)*MAX_MONSTERS);
}

//Remove, as in they're dead. Gone. Ex-raider. 
// Ok, two options here. Null the index or shift everything down.    gNumMonsters gets ugly as fuck if we leave a null. Although we should probably be testing for that anyway. 
void monRemoveForRealzies(int i)
{
  if(gNumMonsters < 1)
  {
    return;
  }

  //Remove mon from map
  //TODO: I think there's a bug with this. I'm seeing corpses on the map. The map says there's a mon index there, but gnumMon is lower and the mon list thinks they're dead
  //   BUT WHATEVER!  now it's a feature. Monster corpses. Yeah
  int x = g_mon[i].x;
  int y = g_mon[i].y;
  g_map[x][y].mon = 0;


  //Decrement number of mons
  gNumMonsters--;

  //Shift all existing mons down.   note: NEVER tie anything to the index of the mon, it can change.   ....except the player... shit.
  for( ; i < gNumMonsters && i < MAX_MONSTERS; i++)
  {
    monCopy(i+1, i);
  }

}
*/



///Moves monsters (not the player) into a new square. 
///@param i index of monster
///@param x,y new target square
///@return 0 = failure to move, 1= successful move, OR an attack
int monMove(int i, int x, int y)
{
  int ret=0;

  if( i >= gNumMonsters || i >= MAX_MONSTERS)
  {
    printf("well something is fucked, your monster index is off\n");
    return 0;
  }

  
  //Change facing based on where they came from
  if(g_mon[i].x == x-1) g_mon[i].facing = EAST;
  if(g_mon[i].x == x+1) g_mon[i].facing = WEST;
  if(g_mon[i].y == y-1) g_mon[i].facing = SOUTH;
  if(g_mon[i].y == y+1) g_mon[i].facing = NORTH;
  //look around randomly
  if(rand()%10 ==0)
    g_mon[i].facing = rand() % NUM_DIRECTIONS;


  //MonsterType specific actions here
  // (Like mons bumping into each other
  switch(g_mon[i].type)
  {
    case MON_TYPE_GUARD:    aiGuardMove(i,x,y); break;   
    case MON_TYPE_PLAYER:
            break;
    case MON_TYPE_NULL:    
    default:
      printf("mon %d, has a weird type\n",i);
  }

  if(isPassable(x,y))
  {
    g_map[g_mon[i].x][g_mon[i].y].mon = 0;
    g_map[x][y].mon = i;
    g_mon[i].x = x;
    g_mon[i].y = y;
    ret=1;
  }
  return ret;
}



int monPickRandomTravelTarget(int i)
{
  int x,y;
  int timeout=0;

  do
  {
    timeout++;
    x = rand()%MAP_SIZE; 
    y = rand()%MAP_SIZE;


    //Make sure they can get to the target
    TCOD_path_t path = TCOD_path_new_using_map(g_tcodMap,0.0f); 
    if(TCOD_path_compute( path, g_mon[i].x, g_mon[i].y, x, y))
    {
      if(TCOD_path_size(path) == 0) 
      { 
        continue;
      }
    }
    TCOD_path_delete(path); 

  } while(!isPassable(x,y) && timeout<10000);  
  g_mon[i].targetX = x;
  g_mon[i].targetY = y;

  if( timeout > 9000) return -1;
  return 0;
}


int monIntComp(const void* a, const void* b)
{
  return (*(int*)a - *(int*)b );
}

/// Find the nearest nth monster to x,y
int monNearestTo(int x, int y, int offset)
{
  int i;
  int list[MAX_MONSTERS][2];
  memset(list, 0, sizeof(int)*MAX_MONSTERS*2);
  for(i=0; i<gNumMonsters ; i++)
  { 
    list[i][1] = i;
    list[i][0] = dist(x,y, g_mon[i].x, g_mon[i].y);
  }
  qsort(list, gNumMonsters, 2*sizeof(int), monIntComp);
  return list[offset][1];

}


