#ifndef __BREAKNECK_AI_GUARD__
#define __BREAKNECK_AI_GUARD__

void aiGuard(int i);

//States
#define AI_GUARD_WANDER   0
#define AI_GUARD_ATTACK   1
//Yeah, that's all they do

#define RESP_IGNORE 0
#define RESP_ATTACK 1


void aiGuardAttack(int i);
void aiGuardWander(int i);

void aiGuardMove(int i, int x, int y);

void aiGuardSpotCheck(int i);
#endif
