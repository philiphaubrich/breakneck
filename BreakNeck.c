/*
  Breakneck. Philip Haubrich 2021/03/6
  Released under GPLv3 
  Stealing AzureRL and continuing off of one day's effort back at some game jam for the 2021 7DRL challenge.   One of these years, I'm goingn to show up to these things on time. 

  Yeah, really only had about ONE DAY to work on that in 2020. One day a 7drl does not make. 
  omg, and I had forgotten there was some minor effort in 2021. Yeah, not much happened there.
  So it's 2022! THE SEQUAL!     ...Aaaaand It's friday already. So yeah, three whole days to work on this. Life is getting better.
    Oooof, but this is the third year really trying to get this going.   siiiiigh, I guess it just takes time to build up momentum. 

TODO LIST: 

+ Chop out everything unwanted from the old game. 
+ regular commits to gitlab. 
+ Build
	+2021 build
+ guard that chases you,
+ Collisions between all objects.  guards: YOU DIED
+ End scene? you lose

+ Yeah, need to re-think input and the implementation to the premise of the game. 
  + You need a map of of what actions go to what square. You can't just loop on speed.
      -(And more like a set of motions to get there, so we travel the path)
  + You really do need to rotate this. It's just math, be not afraid.
      x' = cos(45) * x - sin(45) * y
      y' = sin(45) * x + cos(45) * y
         ...That isn't going to work with a grid...
         uuuuuh....  yeah, some tweaks are needed to deal with corners... TODO still
  + You need a map of what actions affect speed
  + You need a map of what actions affect direction.  ( all combined)
  + Facing, speed, and of course location

 1) Input numpad 1-9, w/facing ->  relative directions like FWD, FWD_L, BACK_R
 2) relative dir, w/speed   -> maps of destination, speed and facing transitions.
 3) Relative destination, 2F 1R  -> X,Y coordinates  (And bresenham to get path)
 4) Speed and facing update happen
 5) A path is made of X steps to execute and advance time before needing user input again.
 6) Path resolves, 
    -With a set delay, so we see what happens in-between
    -Checks happen like impacting guards or walls or stairs.
 And we do it all over again
+ Momentum changes.  stand-still, walk, job, run, breakneck. 
  WHEW. Big one.
 
2022, Sunday. Alright. About 12 hours left. 
+ map-Gen. buildings, chasms, walls. 
X Obstacles: Wall, low walls, things you could slide under. 
    Eh, just walls for now
+ Jumping. 
- levels, "stairs". Generate a new map and put the player at the bottom
- bring guards back
- animation,display you moving through other tiles while running.
  -Cut hard halt in player input
  -Create path from destX, walk path at big tick
    -Collisions
    -Jumping Specifically over walls.
-Color highlight potential movement.  red is dead
  -DAS BLINKEN!
- Look into Full Screen. You ARE going to demo this
-Tagging walls.

? queueing up commands (since you can now spam faster than the game goes)
    -Some bonus for "quick action".   
- Grabbing.  
  -Tiles which are poles you can grab and change course
- 3D splitscreen. side-projection.   All the time? 
- sliding. Under some walls, under guards.
-Experiment with real-time, ie, a timeout waiting for player commands
- Swapping tiles during animation,  ie, jump animation.   differentiate walk/jog/run. 
-  Tick-tock right/left  animation cadence for... you know, running. 
- Facing graphics, as direction is kinda important.
  - right and left are just flipped
  - up and down?    ...Could be forwards and back.  north is implicitly "up".  
  - and have all the buildings like pseudo angle aof view. liiiike, you could make a map that makes it look like you keep climbing up roofs. You could jump and grab ledges, get up over walls. 
-Consider switching to a continuous map that falls down (just do some clever shit with a bit of mobius memory).  the bottom is the monster of time, consuming all. 
    -Chievo of getting all the guards on the map eaten by the monster
    -dying guards. 
    -replacing guards in some way.    timer bringing guards back to a level/threshold.
-(ugh, hand-drawn diagrams are just better for showing the plan of what keys do what to update location, facing, and speed.  Don't waste time putting those down here)
+There's still unresolved questions about how to rotate from lateral to diagonal motion.  
  The answer is "poorly".  I cobbled together some diagrams, but the rotational math decided otherwise.
    What I've got is "close enough" and arguably makes more sense. 
-Experiment with mid-jump actions. Hitting space-bar to advance time. 
-Experiment with 5, COAST, only ever advancing 1 step. 1 unit of time. So you can time jumps instead of going the full 4 squares at breakneck speed and not being able to jump at the righ time for maximum distance
-Sideview when jumping at breakneck speed.   Show the rooftop and pit or whatever.



BUG:
- You can try to move into guards without losing.
- The next stage has a turn where some guards are painted


 if at all possible, try to port to HTML5 and javascript via emscripten. Because no one is going to compile C code to play your shit.



 */

//#include "curses.h" out with the old
#include "map.h"
#include "genMap.h"   //TODO didn't I want complete authority of genMap through map?
#include "mon.h"
#include "paint.h"
#include "ai_guard.h"
#include "playerMovement.h"
#include "BreakNeck.h"
#include "libtcod/libtcod.h"


#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <stdarg.h>

#include <unistd.h>   //Just for sleep
#include <time.h>
//#define _POSIX_C_SOURCE 199309L
//#define _XOPEN_SOURCE   600
//#define _POSIX_C_SOURCE 200112L
///////aaaaaaaaaaaahhhhhhhhhhhhH!!!!!!!!!!!!!!
//#define __USE_POSIX199309
//#define _POSIX_C_SOURCE 199309L


//For unit testing
//extern int test();


//motherfuckin globals! when I'm tired and no longer give any shits
FILE* f_log;
int g_turn=0;
int g_chap=0;
int g_money=0;
int g_YOUDIED=0;

int g_bug[20]={};
char g_bugName[20][20]={};

long int g_tickDelay_ms = 500;
int g_jumping=0;

int getRelativeDirection(int key, int facing );



///Find the distance between two places on the map. Roughly circular.
///IN: 2 x/y coordinates
///RETURN: integer of the distance.
int dist(int x1, int y1, int x2, int y2)
{
  return (int)sqrt((float)((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)));
}


///Make a new map, reset player/monsters
void stageLoad()
{
  int ret=0;
  g_chap++;

 // ret = generateCityBlockMap();
  ret = generateBigRooftopMap(g_chap);
  if( ret != 0 )
  {
	  printf("error making maps, bailing %d\n", ret);
  }

  clearMonsters();
  monGenerateMonstersRandom(3 + g_chap*2); 

  monPlacePlayer(MAP_SIZE/2,MAP_SIZE-2);
  consoleAdd("StageLoaded");
}




#define ADVANCE_TIME 1
#define ADVANCE_PLAYER 2
#define ADVANCE_MONSTERS 3
#define ADVANCE_ANIMATION 4
///Mostly just moves monsters around at this point
int advanceTime(int advanceWhatNow)
{
  int ret;
  switch(advanceWhatNow)  //ALL GAS ! NO BREAKS! 
  {
    case ADVANCE_MONSTERS:
      //control all the guards
      monProcessAI();
    case ADVANCE_PLAYER:
      //Adance down player's path
      ret = oneStepThroughPlayerPath(); 
    case ADVANCE_TIME:
      g_turn++;
    case ADVANCE_ANIMATION:
      //I dunno some tick-tock animation, like you're jogging
    default:
      ;
      //sleep(1);
  }

  //Bring out your dead!   Yeah, so it's really important that we NOT fuck with the 
  //  monster index until everything else is done.
  //monCorpseCart();   //Somethign is up there. No dyign for now  TODO: dead guards, if that's even needed
  return ret;
}



///Eats keyboard commands
///HALTS THE PROGRAM
int userInput()
{
  TCOD_key_t key;

  //TODO: We're going to have to change the halting behaviour to slide through every square on the way to our target instead of teleporting there.
  //TCOD_mouse_t mouse;
  //TCOD_event_t ev = TCOD_sys_check_for_event(TCOD_EVENT_ANY,&key,&mouse);
  //if ( ev == TCOD_EVENT_KEY_PRESS && key.c == 'i' ) { ... open inventory ... }


  key = TCOD_console_wait_for_keypress(true);

  //I want my numpad
  if(key.vk >= TCODK_KP0 && key.vk <= TCODK_KP9)
  {
    key.c = '0'+ key.vk - TCODK_KP0; //haaaaaaack cause I'm lazy
  }
     

  //printf("key: %c\n",key.c);
  switch(key.c)
  {
    case 'q':
    case 'Q':
      return QUIT;
      break;
   /*
    case 'w':  g_mon[0].facing = NORTH; break;
    case 'e':  g_mon[0].facing = NORTHEAST; break;
    case 'a':g_mon[0].facing =  WEST; break;
    case 's':g_mon[0].facing = SOUTH; break;
    case 'd':g_mon[0].facing = EAST; break;
    */
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
    case 'j':
    case 'J':
      playerMovementInput(key.c);
      return ADVANCE;
      break;
    //case ' ':
    //case 'b':
    //  wtf();
    //  printf("break here\n");//It's just for debugging
    //case '.':
    //  return ADVANCE; //tmp?
    //  break;
    case 'u':
      g_tickDelay_ms = g_tickDelay_ms/2;
      break;
    case 'h':
      g_tickDelay_ms = g_tickDelay_ms*2;
      break;
    default:
      return 0;
  }

  return 0;
}



///Saves turn actions to file. For debugging. 
void logging()
{
  int i;
  fprintf(f_log,"turn: %d\n",g_turn);
  for(i=0; i < MAX_MONSTERS && i <= gNumMonsters; i++)
  {
    fprintf(f_log,"%d @ %d,%d\t",i, g_mon[i].x, g_mon[i].y);
  } 
  fprintf(f_log,"\n");  
}



////Initialize libtcod stuff: Map, console, window,
void initlibtcod()
{
  //int TCOD_console_set_custom_font(const char *fontFile, int flags, int nb_char_horiz, int nb_char_vertic)
  //TCOD_console_set_custom_font("ArtAndStuff/tiles1280x1280.png", TCOD_FONT_LAYOUT_ASCII_INCOL,  80,80);
  //TCOD_console_set_custom_font("ArtAndStuff/tiles32.png", TCOD_FONT_LAYOUT_ASCII_INCOL,  32,32);
  //TCOD_console_set_custom_font("ArtAndStuff/tiles32.bmp", TCOD_FONT_LAYOUT_ASCII_INCOL,  32,32);

  if(TCOD_console_init_root (MAP_SIZE+30, MAP_SIZE, "BreakNeck", 0, TCOD_RENDERER_SDL) != 0)
  {
    printf("libtcod is fucked, do you have the tiles?\n");
    exit(-1);
  }
  TCOD_console_set_default_background(NULL, TCOD_black);
  TCOD_console_set_default_foreground(NULL, TCOD_white);
  TCOD_console_clear(NULL);

  //TCOD_console_credits(); TODO make debug/final builds to turn this sort of stuff on.
}


/*
/// Older initialize for the PD curses library. Switched to libTCOD which uses SDL 
void initPDcurses()
{
initscr();
resize_term(SCREEN_SIZE_X,SCREEN_SIZE_Y); 
//WINDOW *resize_window(WINDOW *, int, int);
getmaxyx(stdscr, LINES, COLS);
raw();
cbreak(); //don't wait on /r /n
noecho();
nodelay(stdscr, FALSE); //getch() HALTS the program
//nodelay(stdscr, TRUE); //getch() Does not HALT the program
//keypad(stdscr, TRUE);
curs_set(0);
return;
}
 */



///Sets up game data. 
///When the map or monsers need to be reset, they have alternative functions. 
///This just happens at boot
void initGameData()
{
  srand(abs(time(NULL)));
  g_YOUDIED = 0;
  g_money = 0;
  g_turn = 0;
  monInitPlayer();
  initMotionKeys();

  stageLoad();
}

int stairCheck()
{
  int x = g_mon[0].x;
  int y = g_mon[0].y;
  if(g_map[x][y].special == TILE_UPSTAIR)
  {
    stageLoad();
    paintScreen(g_turn);
    return 1;
  }
  return 0;
}

///Initializes, loops with a halt, updates world, and paints screen after user action.
int main(int argc, char** argv)
{
#ifdef TEST
  test();
  return 0;
#endif

  int action=0;
  int userInputNeeded=1;
  initlibtcod();
  initGameData();

  f_log = fopen("log.txt","w");


  while(action != QUIT)
  {
    logging();

    if(action == ADVANCE)
      userInputNeeded = advanceTime(ADVANCE_MONSTERS);

    playerPotentialMovement(); //populates keyMotions, used in paint
    paintScreen(g_turn);

    if( stairCheck()) continue;



    if(g_YOUDIED)
    {
      paintEnding();
      initGameData();
    }

    //if(userInputNeeded)
    if(g_currentStep == g_quedStep)
    {
      g_jumping = 0; //FUCKIFIKNOW!!!
      action = userInput(); //HALTS
    }
    else
    {
      //sleep(1);
//      usleep(g_tickDelay);

      struct timespec ts;
      ts.tv_sec = 0;
      ts.tv_nsec = ( g_tickDelay_ms % 1000) * 1000000;
      nanosleep(&ts, NULL);
    }
  }	

  fclose(f_log);
  return EXIT_SUCCESS;
}  



void wtf()
{
  printf("Yeah, wtf?\n");
}
