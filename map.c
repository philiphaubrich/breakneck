/** map.c
In charge of storing information about the play area, and dealing with it's interactions
*/
#include "map.h"
#include "mon.h"
#include "genMap.h"
#include "BreakNeck.h"
#include "libtcod/libtcod.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>




struct Map_t g_map[MAP_SIZE][MAP_SIZE];
TCOD_map_t g_tcodMap;
TCOD_map_t g_tcodMapMonView;



char inMap(int x, int y)
{
  if( x >= MAP_SIZE || x < 0 || y >= MAP_SIZE || y < 0 )
    return 0;
  return 1;
}

int isVisionblocking(int x, int y)
{
  if(!inMap(x,y) || g_map[x][y].isWall)
  {
    return 1;
  }
  return 0;
}



//Glory be to Bresenham!
int hasLOS(int x1, int y1, int x2, int y2)
{
  int dx, dy, i, e;
  int incx, incy, inc1, inc2;
  int x,y;

  if(!inMap(x1,y1) || !inMap(x2,y2))
    return 0;

  dx = x2 - x1;
  dy = y2 - y1;

  if(dx < 0) { dx = -dx; }
  if(dy < 0) { dy = -dy; }
  incx = 1;
  if(x2 < x1) { incx = -1; }
  incy = 1;
  if(y2 < y1) { incy = -1; }
  x=x1;
  y=y1;

  if(dx > dy)
  {
    e = 2*dy - dx;
    inc1 = 2*( dy -dx);
    inc2 = 2*dy;
    for(i = 0; i < dx-1; i++)
    {
      if(e >= 0)
      {
        y += incy;
        e += inc1;
      }
      else { e += inc2; }
      x += incx;

      if(isVisionblocking(x,y))
        return 0;
    }
  }
  else
  {
    e = 2*dx - dy;
    inc1 = 2*( dx - dy);
    inc2 = 2*dx;
    for(i = 0; i < dy-1; i++)
    {
      if(e >= 0)
      {
        x += incx;
        e += inc1;
      }
      else { e += inc2; }
      y += incy;
      if(isVisionblocking(x,y))
        return 0;
    }
  }
  return 1;
}




int isPassable(int x, int y)
{
  if(!inMap(x,y) || g_map[x][y].isWall || g_map[x][y].mon  
    || (g_mon[MON_PLAYER].x == x && g_mon[MON_PLAYER].y == y) )   
    //Hmmmmm, should monsters and the player be passable?
    return 0;
  else
    return 1;
}

float isTcodPassible(int xFrom, int yFrom, int xTo, int yTo, void *user_data)
{
  return (float)(isPassable(xTo, yTo));   
}




void mapSaveToFile()
{
}
