#ifndef __BREAKNECK_MAP__
#define __BREAKNECK_MAP__

#include "BreakNeck.h"
#include "libtcod/libtcod.h"

//Special tiles
#define TILE_UPSTAIR    '>'   //Ends level, advances plot, makes new map, whatnot
#define TILE_DOWNSTAIR  '<'   // So far, NOTHING
#define TILE_MONEY      '$'   // Which serves no real purpose. So at least we've got historical accuracy. 
#define TILE_BAR        'B'   // Bars / grabbable things.  Railing?
#define TILE_NOTHING     0

//Global data.  Cause we like being aware of things. 
extern Map_t g_map[MAP_SIZE][MAP_SIZE];
extern TCOD_map_t g_tcodMap;
extern TCOD_map_t g_tcodMapMonView;

void mapInit();
char inMap(int x, int y);
int isPassable(int x, int y);
//void mapLoadFromFile();
//void mapSaveToFile();


#endif
