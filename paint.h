#ifndef __BREAKNECK_PAINT__
#define __BREAKNECK_PAINT__

#include "BreakNeck.h"
#include "libtcod/libtcod.h"


#define VIEW_NIGHT    0
#define VIEW_DAY      1
#define VIEW_MONSTER  2
#define MAXVIS        3
//Initially, the players need to see everything so they know what's going on.
//In later levels, have them sneak out at night with fog of war


//common scenes that aren't chapters.
// hmmmmm, maybe I do want scenes
#define SCENE_DEATH   -3



void monsterFOV(int i, int directionalView);

void paintScreen(int turn);
void paintEnding();
void scene(int chap);
void consolePrint();
void consoleAdd(const char* str);


void printMap_playerView();  //This being here is... questionable. 

void printPathOverlay(int i);

void printHUDmonStats(int x, int y, int offset);
#endif
